//
//  TextFontStyle.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

enum TextFontStyle: String {
    case normal
    case bold
    case light
    case medium
    
    init(rawValue: String) {
        switch rawValue {
        case "normal":
            self = .normal
        case "bold":
            self = .bold
        case "light":
            self = .light
        case "medium":
            self = .medium
        default:
            self = .normal
        }
    }
    
    func textFont(size: CGFloat) -> UIFont {
        switch self {
        case .normal:
            return UIFont.systemFont(ofSize: size)
        case .bold:
            return UIFont.boldSystemFont(ofSize: size)
        case .light:
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.light)
        case .medium:
            return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.medium)
        }
    }
}
