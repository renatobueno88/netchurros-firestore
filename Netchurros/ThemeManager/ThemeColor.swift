//
//  ThemeColor.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

enum ThemeColor: String {
    
    case blackColor
    case promotionColor
    case errorColor
    case whiteColor
    case darkGrayColor
    case purchaseColor
    case disabledColor
    case primaryColor
    case secondaryColor
    case tertiaryColor
}
