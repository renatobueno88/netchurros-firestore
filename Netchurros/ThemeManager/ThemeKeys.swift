//
//  ThemeKeys.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

enum ThemeKeys: String {
    case navigationBarStyle
    case priceLabelStyle
    case descriptionLabelStyle
    case titleLabelStyle
    case promotionLabelStyle
    case toolBarButtonsStyle
    case purchaseButtonStyle
}
