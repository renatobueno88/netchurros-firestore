//
//  UICollectionView+extension.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 24/08/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func calculateSizeCell(numberOfCells: Float, cellSpace: CGFloat?) -> CGSize {
        let screenValueToCalculate = 0.9465240642
        var viewWidth = self.frame.size.width
        if let cellSpace = cellSpace {
            viewWidth -= cellSpace
        }
        let width = viewWidth > 0 ? Double(viewWidth / CGFloat(numberOfCells)) : Double(1 / CGFloat(numberOfCells))
        
        let height = Double(width * screenValueToCalculate + 105)
        return CGSize(width: width, height: height)
    }
    
    func animateCellExibition(atIndex indexPath: IndexPath, view: UIView, duration: TimeInterval = 0.3, completion: ((Bool) -> Void)? = nil) {
        view.alpha = 0
        UIView.animate(withDuration: duration, delay: (0.05 * Double(indexPath.row)), animations: {
            view.alpha = 1
        }, completion: completion)
    }
}
extension UICollectionViewCell {
    class func createCell<T: UICollectionViewCell>(collectionView: UICollectionView, indexPath: IndexPath) -> T {
        return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: T.self), for: indexPath) as! T
    }
}

struct CoreSetup {
    
    static var currentOsVersion: String {
        return UIDevice.current.systemVersion
    }
}

