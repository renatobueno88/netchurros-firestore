//
//  UILabel+extensions.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

extension UILabel {
    func setTheme(withStyle style: ThemeKeys) {
        guard let dto = ThemeManager.getThemeForLabel(withStyle: style) else {
            return
        }
        backgroundColor = dto.backgroundColor
        layer.cornerRadius = dto.borderRadius
        layer.borderWidth = dto.borderWidth
        layer.borderColor = dto.borderColor.cgColor
        textColor = dto.textColor
        font = dto.textFont
    }
}
