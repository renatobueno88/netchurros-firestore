//
//  UIControllers+extensions.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

extension UIViewController {
    @objc func consistToTheme() {
        self.children.forEach { $0.consistToTheme() }
    }
}

extension UINavigationController {
    open override func viewDidLoad() {
        super.viewDidLoad()
        consistToTheme()
    }
    
    override func consistToTheme() {
        super.consistToTheme()
        if let style = ThemeManager.getThemeForNavigationBar() {
            navigationBar.isTranslucent = false
            navigationBar.barTintColor = style.barColor
            navigationBar.tintColor = .white
            navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: style.textColor]
        }
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
}

