//
//  ProductImageTableViewCell.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 09/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

final class ProductImageTableViewCell: UITableViewCell {

    private lazy var productImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 14
        imageView.contentMode = .scaleAspectFill
        imageView.clearsContextBeforeDrawing = true
        imageView.autoresizesSubviews = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImage(image: UIImage) {
        productImageView.image = image
    }
    
    // MARK: LAYOUT SETUP
    
    private func setupView() {
        addSubview(productImageView)
    }
    
    private func setupConstraints()  {
        let margins: CGFloat = 12
        let topBottomMargins: CGFloat = 6
        NSLayoutConstraint.activate([
            productImageView.heightAnchor.constraint(equalTo: productImageView.widthAnchor, multiplier: (1.0 / 1.0)),
            productImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: margins),
            productImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -margins),
            productImageView.topAnchor.constraint(equalTo: topAnchor, constant: topBottomMargins),
            productImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -topBottomMargins)
            ])
    }
}
