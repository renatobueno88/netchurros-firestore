//
//  TextTableViewCell.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 09/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

struct TextCellDto {
    var text = ""
    var theme = ThemeKeys.titleLabelStyle
}

final class TextTableViewCell: UITableViewCell {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fill(dto: TextCellDto) {
        setTheme(style: dto.theme)
        titleLabel.text = dto.text
        
    }
    
    // MARK: LAYOUT SETUP
    
    private func setTheme(style: ThemeKeys) {
        titleLabel.setTheme(withStyle: style)
    }
    
    private func setupView() {
        addSubview(titleLabel)
    }
    
    private func setupConstraints()  {
        let margins: CGFloat = 12
        let topBottomMargins: CGFloat = 6
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: margins),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -margins),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: topBottomMargins),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -topBottomMargins)
            ])
    }

}
