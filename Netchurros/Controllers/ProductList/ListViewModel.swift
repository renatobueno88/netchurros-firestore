//
//  ListViewModel.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 18/08/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

final class ListViewModel {
    
    private var churros: [ChurrosObject]?
    
    init() {
    }
    
    func fetchData() {
        churros = fetchChurros()
    }
    
    func numberOfRows(section: Int) -> Int {
        return churros?.count ?? 0
    }
    
    func numberOfSections() -> Int {
        return churros != nil ? 1 : 0
    }
    
    func getProductListDTO(atIndex index: IndexPath) -> ProductListDTO {
        guard let churro = churros, !churro.isEmpty else {
            return ProductListDTO()
        }
        let churroObject = churro[index.row]
        return ProductListDTO(title: churroObject.title,
                              imageNamed: churroObject.image,
                              price: churroObject.price.currencyValue,
                              shouldShowFavorite: shouldShowFavorite)
    }
    
    func getSelectedChurro(atIndex index: IndexPath) -> ChurrosObject? {
        guard let churros = churros else {
            return nil
        }
        return churros[index.row]
    }
    
    private var shouldShowFavorite: Bool {
        return ToggleRequestManager.toggleIsEnabled(forKey: .favoriteIsEnabled) ?? false
    }
    
}
