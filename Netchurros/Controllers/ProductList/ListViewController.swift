//
//  ListViewController.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 18/08/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

typealias CollectionViewDelegateDataSource = UICollectionViewDelegate & UICollectionViewDataSource

final class ListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var productCellIdentifier: String {
        return String(describing: ProductListCollectionViewCell.self)
    }
    
    lazy var viewModel = ListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        fetchData()
    }
    
    private func fetchData() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewModel.fetchData()
            self.collectionView.reloadData()
        }
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(ProductListCollectionViewCell.self, forCellWithReuseIdentifier: productCellIdentifier)
    }
    
    private func createCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProductListCollectionViewCell = UICollectionViewCell.createCell(collectionView: collectionView, indexPath: indexPath)
        cell.layer.cornerRadius = 4
        cell.fill(dto: viewModel.getProductListDTO(atIndex: indexPath))
        return cell
    }
    
    // MARK: NAVIGATION
    
    func showChurroDetail(withChurro selectedChurro: ChurrosObject) {
        let controller = DetailControllerBuilder(fromController: self.navigationController, dataSource: selectedChurro)
        DispatchQueue.main.async {
            controller.buildAndPresent()
        }
    }
    
}
extension ListViewController: CollectionViewDelegateDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return createCell(collectionView, cellForItemAt: indexPath)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        collectionView.animateCellExibition(atIndex: indexPath, view: cell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedChurro = viewModel.getSelectedChurro(atIndex: indexPath) else {
            return
        }
        showChurroDetail(withChurro: selectedChurro)
    }
}
extension ListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView.calculateSizeCell(numberOfCells: 2, cellSpace: nil)
    }
}
