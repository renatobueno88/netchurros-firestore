//
//  FeatureToggle.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 09/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

enum ToggleKey: String, Codable {
    case favoriteIsEnabled
    case descriptionEnabled
    case promotionEnabled
}

struct FeatureToggle: Codable {
    
    var toggleKey: ToggleKey?
    private(set) var toggleName = ""
    var enabled: Bool = false
    var disabledOs = [String]()
    
}
