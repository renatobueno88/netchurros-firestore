//
//  Theme.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 10/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

struct Theme: Codable {
    
    var styles: Styles   
}

struct Styles: Codable {
    var ios: [ThemeStyles]
    var colorProperties: [ThemeColors]
}

typealias ThemeColors = StyleProperties

struct ThemeStyles: Codable {
    var styleName: String
    var styleProperties: [StyleProperties]
}

struct StyleProperties: Codable {
    var key: String
    var value: String
}


