//
//  ToggleRequestManager.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 09/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

struct ToggleRequestManager {
    
    static private var toggles: [FeatureToggle]?
    
    static func fetchToggleObjects() {
        FirebaseFireStoreLoader().listenToNode(withKey: .toggles, collectionKey: "features", type: [FeatureToggle].self) { (toggle, _) in
            self.toggles = toggle
        }
    }
    
    private static func currentOperationalSystemIsValid(forToggle toggle: FeatureToggle?) -> Bool {
        let currentOs = CoreSetup.currentOsVersion
        return !(toggle?.disabledOs.contains(currentOs) ?? false)
    }
    
    static func toggleIsEnabled(forKey key: ToggleKey) -> Bool? {
        let toggle = ToggleRequestManager.toggles
        
        let singleToggle = toggle?.first(where: { $0.toggleName == key.rawValue })
        guard currentOperationalSystemIsValid(forToggle: singleToggle) else {
            return false
        }
        return singleToggle?.enabled
    }
    
}
