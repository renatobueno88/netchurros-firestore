//
//  FirebaseRemoteConfigLoader.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 11/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation
import FirebaseRemoteConfig

enum FirebaseRemoteConfigKey: String {
    case themes
    case promotionName
}

class FirebaseRemoteConfigLoader: JSONDecodable {
    
    static let sharedInstance = FirebaseRemoteConfigLoader()
    private let remoteConfig = RemoteConfig.remoteConfig()
    
    private init() {
        configureDefaultSettings()
        load(defaultsResource: "Theme")
        fetchCloudValues()
    }
    
    private func load(defaultsResource: String) {
        remoteConfig.setDefaults(getLocalJson(resource: defaultsResource))
    }
    
    private func fetchCloudValues() {
        // WARNING: AVOID THIS SHORT TIME EXPIRATION DURATION IN PRODUCTION CODE
        let duration: TimeInterval = 0
        remoteConfig.fetch(withExpirationDuration: duration) { (status, error) in
            self.treatFetch(status: status, error: error)
        }
    }
    
    private func treatFetch(status: RemoteConfigFetchStatus, error: Error?) {
        guard error == nil else {
            print("REMOTE CONFIG ERROR", error)
            return
        }
        remoteConfig.activateFetched()
    }
    
    private func getLocalJson(resource: String) -> [String: NSObject] {
        let path = Bundle.main.path(forResource: resource, ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        let jsonResult = try! JSONSerialization.jsonObject(with: data, options: []) as! [String: NSObject]
        return jsonResult
    }
    
    private func configureDefaultSettings() {
        let settings = RemoteConfigSettings()
        remoteConfig.configSettings = settings
    }
    
    func getStringValue(fromKey key: FirebaseRemoteConfigKey) -> String? {
        return remoteConfig[key.rawValue].stringValue
    }
    
    func getJsonResponse<T: Codable>(fromKey key: FirebaseRemoteConfigKey, type: T.Type, completion: @escaping (_ value: T?, _ error: Error?) -> Void) {
        let configData = remoteConfig[key.rawValue].dataValue
        decodeJson(fromData: configData, completion: completion)
    }
}

