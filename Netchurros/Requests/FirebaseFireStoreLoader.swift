//
//  FirebaseFireStoreLoader.swift
//  Netchurros
//
//  Created by Renato Souza Bueno on 09/09/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation
import FirebaseFirestore

enum FirebaseFireStoreKey: String {
    case toggles = "toggles/iOS"
}

protocol FirebaseFireStoreRequestable {
    func listenToNode<T: Codable>(withKey key: FirebaseFireStoreKey, collectionKey: String, type: T.Type, completion: @escaping (T?, Error?) -> ())
}

class FirebaseFireStoreLoader: FirebaseFireStoreRequestable, JSONDecodable {
   
    private let firestore = Firestore.firestore()
    
    init() {
    }
    
    func listenToNode<T: Codable>(withKey key: FirebaseFireStoreKey, collectionKey: String, type: T.Type, completion: @escaping (T?, Error?) -> ()) {
        firestore.document(key.rawValue).collection(collectionKey).addSnapshotListener { (snapshot, error) in
            guard let snapshot = snapshot, let data = self.serializeSnapshotToData(snapshot: snapshot) else {
                completion(nil, error)
                return
            }
            self.decodeJson(fromData: data, completion: completion)
        }
    }
    
    private func serializeSnapshotToData(snapshot: QuerySnapshot) -> Data? {
        let data = try? JSONSerialization.data(withJSONObject: snapshot.documents.map { $0.data() }, options: [])
        return data
    }
}
