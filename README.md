# Netchurros-Firestore

Example app used on a Cocoaheads Brazil iOS talk about Feature Toggle (feature flag) and Layout Themes/Styles dynamically using Firebase as example. 

**Frameworks used with Cocoapods dependency manager:**

* Firebase RemoteConfig
* Firebase Cloud FireStore

**Note:** (unfortunately) to install Firebase on your app, the FirebaseCore framework also import some other frameworks that are necessary to build the app using Firebase.

# Presentation link (in brazilian-portuguese): 

http://tiny.cc/w15sdz